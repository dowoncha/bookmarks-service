import { Injectable } from '@nestjs/common';

import { User } from './user.interface'

@Injectable()
export class UsersService {
  private readonly users: User[]

  constructor() {
    this.users = [
      {
        id: "1",
        username: "kirk",
        password: "password"
      },
      {
        id: "2",
        username: "picard",
        password: "password"
      },
      {
        id: "3",
        username: "kirk@starfleet.com",
        password: "password"
      }
    ]
  }

  async findOne(username: string): Promise<User | undefined> {
    return this.users.find(user => user.username === username)
  }
}
