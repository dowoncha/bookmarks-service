import { Test, TestingModule } from '@nestjs/testing';
import { LocalStrategy } from './local.strategy'
import { AuthService } from './auth.service'
import { UsersModule } from '../users/users.module'

describe('LocalStrategy', () => {
  let service: LocalStrategy;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [UsersModule],
      providers: [AuthService, LocalStrategy],
    }).compile();

    service = module.get<LocalStrategy>(LocalStrategy);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

