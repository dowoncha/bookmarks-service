import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { ApiController } from './api.controller'
import { AuthModule } from './auth/auth.module';
import { BookmarksModule } from './bookmarks/bookmarks.module';

@Module({
  imports: [AuthModule, BookmarksModule],
  controllers: [ApiController],
})
export class AppModule {}
