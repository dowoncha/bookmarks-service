import { Controller, Get, Post, Req, Param, Body, Delete } from '@nestjs/common'
import { Request } from 'express'

import { Bookmark } from './bookmark.interface'
import { BookmarksService } from './bookmarks.service'

export class CreateBookmarkDto {
  readonly name: string
  readonly description: string
  readonly tags: string[]
  readonly url: string
}

@Controller('api/bookmarks')
export class BookmarksController {
  constructor(private readonly bookmarksService: BookmarksService) {}

  @Get()
  async findAll(@Req() request: Request): Promise<Bookmark[]> {
    return this.bookmarksService.findAll()
  }

  @Get(":id")
  findById(@Param() params): string {
    return `This action returns a #${params.id} bookmark`
  }

  @Post()
  create(@Body() createBookmarkDto: CreateBookmarkDto) {
    this.bookmarksService.create(createBookmarkDto)
  }

  @Delete(":id")
  remove(@Param() params): string {
    return `This action removes a #${params.id} bookmark`
  }
}