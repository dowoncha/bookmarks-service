import { Injectable } from '@nestjs/common'
import { Bookmark } from './bookmark.interface'

@Injectable()
export class BookmarksService {
  private readonly bookmarks: Bookmark[] = []

  private static ID = 0

  create(bookmark: Bookmark) {
    const id = ++BookmarksService.ID

    this.bookmarks.push({ id: id.toString(), ...bookmark })
  }

  findAll(): Bookmark[] {
    return this.bookmarks
  }
}