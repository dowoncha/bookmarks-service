export interface Bookmark {
  id?: string,
  name?: string,
  description?: string,
  tags?: string[],
  url: string
}